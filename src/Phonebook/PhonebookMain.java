package Phonebook;

import java.util.*;

public class PhonebookMain {

    public static void main(String[] args) {
        //System.out.println("Welcome to phonebook! You can: \n 1. Add new contact \n 2. Find phone number by the name");
        PhoneBook myBook = new PhoneBook();
        Contact user = new Contact("88005553535", "Meldinya");
        Contact user1 = new Contact("8800", "Melia");
        Contact user2 = new Contact("666666", "Nicolle");
        myBook.add(user);
        myBook.add(user1);
        myBook.add(user2);
        //myBook.getByPhoneNumber("8800");
        System.out.println(myBook.toString());
        System.out.println(myBook.getByName("Melia"));

    }
}





