package Phonebook;

import java.util.*;

class PhoneBook {
    private Map<String, Set<Contact>> phoneNumberMap = new HashMap<>();
    private Map<String, Set<Contact>> nameMap = new HashMap<>();

    public void add(Contact entry) {
        Set<Contact> set
                = phoneNumberMap.computeIfAbsent(entry.phoneNumber, k -> new HashSet<>());
        set.add(entry);

        set = nameMap.computeIfAbsent(entry.name, k -> new HashSet<>());
        set.add(entry);
    }

    public Set<Contact> getByPhoneNumber(String phoneNumber) {
        return phoneNumberMap.get(phoneNumber);
    }

    public Set<Contact> getByName(String name) {
        return nameMap.get(name);
    }

    public static boolean checkValueExists(Map<String, Object> eMap, String searchedValue){
        return eMap.values().parallelStream()
                .flatMap(set -> ((Set<String>)set).stream())
                .anyMatch(set -> set.contains(searchedValue));
    }

    @Override
    public String toString() {
        List<Map.Entry<String, Set<Contact>>> list = new ArrayList<>(nameMap.entrySet());
        return list.toString();
    }
}