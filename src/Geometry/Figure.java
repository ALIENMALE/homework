package Geometry;

abstract class Figure {
    double x;
    double y;

    Figure (double x, double y){
        this.x = x;
        this.y = y;
    }

    abstract double getPerimeter();
    abstract double getArea();
    abstract void showMe();
}

class Point extends Figure {

    Point(double x, double y) {
        super(x, y);
    }

    @Override
    double getPerimeter() {
        return 0;
    }

    @Override
    double getArea() {
        return 0;
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Point length is " + getPerimeter());
        System.out.println("Point area is " + getArea());
    }
}

class Line extends Figure {
    double xEnd;
    double yEnd;

    Line(double x, double y, double xEnd, double yEnd) {
        super(x, y);
        this.xEnd = xEnd;
        this.yEnd = yEnd;
    }

    @Override
    double getPerimeter() {
        return Math.sqrt((Math.pow(xEnd, 2) - Math.pow(x, 2)) + (Math.pow(yEnd, 2) - Math.pow(y, 2)));
    }

    @Override
    double getArea() {
        return 0;
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Line length is " + getPerimeter());
        System.out.println("Line area is " + getArea());
    }
}

class Circle extends Figure {
    double radius;

    Circle(double x, double y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    double getArea() {
        return Math.PI * Math.pow(radius,2);
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Circle perimeter is " + getPerimeter());
        System.out.println("Circle area is " + getArea());
    }
}

class Triangle extends Figure {
    double frstSide;
    double scndSide;
    double thrdSide;

    Triangle(double x, double y, double frstSide, double scndSide, double thrdSide) {
        super(x, y);
        this.frstSide = frstSide;
        this.scndSide = scndSide;
        this.thrdSide = thrdSide;
    }

    @Override
    double getPerimeter() {
        return frstSide + scndSide + thrdSide;
    }

    @Override
    double getArea() {
        double hlfPrmtr = getPerimeter()/2;
        return Math.sqrt(hlfPrmtr * (hlfPrmtr-frstSide) * (hlfPrmtr-scndSide) * (hlfPrmtr-thrdSide));
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Triangle perimeter is " + getPerimeter());
        System.out.println("Triangle area is " + getArea());
    }
}

class Square extends Figure{
    double width;

    Square(double x, double y, double width) {
        super(x, y);
        this.width = width;
    }

    @Override
    double getPerimeter() {
        return width*4;
    }

    @Override
    double getArea() {
        return Math.pow (width, 2);
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Square perimeter is " + getPerimeter());
        System.out.println("Square area is " + getArea());
    }
}

class Rectangle extends Square{
    double height;

    Rectangle(double x, double y, double width, double height) {
        super(x, y, width);
        this.height = height;
    }

    @Override
    double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    double getArea() {
        return width * height;
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Rectangle perimeter is " + getPerimeter());
        System.out.println("Rectangle area is " + getArea());
    }
}

class Prllgrm extends Rectangle {
    double angle;

    Prllgrm(double x, double y, double height, double width, double angle) {
        super(x, y, width, height);
        this.angle = angle;
    }

    @Override
    double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    double getArea() {
        double area = width * height * Math.sin(Math.toRadians(angle));
        return Math.abs(area);
    }

    @Override
    void showMe() {
        System.out.println("------------------------------------------");
        System.out.println("Parallelogram perimeter is " + getPerimeter());
        System.out.println("Parallelogram area is " + getArea());
    }
}


