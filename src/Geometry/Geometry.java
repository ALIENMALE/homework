package Geometry;

import java.util.ArrayList;
import java.util.Arrays;

public class Geometry {
    public static void main(String[] args) {

        Point point = new Point(2,3);

        Line line = new Line (1,1,4,4);

        Circle circle = new Circle(5,5,4);

        Triangle triangle = new Triangle(1,1,3, 4, 5);

        Square square = new Square(3,6,4);

        Rectangle rectangle = new Rectangle(1,1,3, 10);

        Prllgrm prllgrm = new Prllgrm(1,1,4, 8, 45);

        ArrayList<Figure> figures = new ArrayList<>();
        figures.add(point);
        figures.add(line);
        figures.add(circle);
        figures.add(triangle);
        figures.add(square);
        figures.add(rectangle);
        figures.add(prllgrm);

        for (Figure figure : figures) {
            figure.showMe();
        }

    }

}
