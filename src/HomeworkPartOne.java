import java.util.*;

public class HomeworkPartOne {
    //1. Создать пустой проект в IntelliJ IDEA и прописать метод main();
    public static void main(String[] args) {
        //2. Создать переменные всех пройденных типов данных, и инициализировать их значения;
        //primitive types
        byte one = 86;
        short two = 32000;
        int peopleCountPenza = 516450;
        long peopleCount2100 = 14818000000L;
        float fishPercent = 87.9F;
        double cash = 356228.87;
        char bookmark = 'z';
        boolean trustMe = true;
        //strings
        String str = "Nyan Cat go!";
        StringBuilder superStr = new StringBuilder("Nyan Dog go!");
        //arrays
        int[] customersCount = new int[59];
        int[] friends = {26, 27, 59, 69, 84, 96};
        String[][] strArray = new String[10][10];

//        abcd();
//        twoNumbers();
//        isPositive();
//        isNegative();
//        uName();
//        leapYear();

    }

    //3. Написать метод, вычисляющий выражение a * (b + (c / d)) и возвращающий результат,
    // где a, b, c, d – входные параметры этого метода;
    public static void abcd () {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a: ");
        float a = in.nextFloat();
        System.out.print("Input b: ");
        float b = in.nextFloat();
        System.out.print("Input c: ");
        float c = in.nextFloat();
        System.out.print("Input d: ");
        float d = in.nextFloat();
        float result = a*(b+(c/d));
        System.out.printf("a * (b + (c / d)) = %.2f ", result);
    }

    //4. Написать метод, принимающий на вход два числа, и проверяющий что их сумма лежит в пределах
    // от 10 до 20(включительно), если да – вернуть true, в противном случае – false;
    public static void twoNumbers (){
        Scanner in = new Scanner(System.in);
        System.out.print("Input first number: ");
        float firstNum = in.nextFloat();
        System.out.print("Input second number: ");
        float secondNum = in.nextFloat();
        float sum = firstNum + secondNum;
        if ((sum >= 10) && (sum <=20)) {
            System.out.print("true");
        }
        else {
            System.out.print("false");
        }
    }

    //5. Написать метод, которому в качестве параметра передается целое число, метод должен напечатать в консоль
    // положительное ли число передали, или отрицательное;
    // Замечание: ноль считаем положительным числом.
    public static void isPositive() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input integer number: ");
        int number = in.nextInt();
        if (number >= 0) {
            System.out.print("It's positive!");
        }
        else {
            System.out.print("It's negative!");
        }
    }

    //6. Написать метод, которому в качестве параметра передается целое число, метод должен вернуть true,
    // если число отрицательное
    public static void isNegative(){
        Scanner in = new Scanner(System.in);
        System.out.print("Input integer number: ");
        int number = in.nextInt();
        if (number < 0) {
            System.out.print("true");
        }
    }

    //7. Написать метод, которому в качестве параметра передается строка, обозначающая имя, метод должен вывести в
    // консоль сообщение «Привет, указанное_имя!»;
    public static void uName(){
        Scanner in = new Scanner(System.in);
        System.out.print("Пожалуйста, введите свое имя, прежде чем мы начнём: ");
        String name = in.nextLine();
        System.out.printf("Привет, %s !", name);
    }

    //8. * Написать метод, который определяет является ли год високосным, и выводит сообщение в консоль.
    // Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й – високосный.
    public static void leapYear(){
        Scanner in = new Scanner(System.in);
        System.out.print("I can help you determine the leap year. Please, input year: ");
        int year = in.nextInt();
        if ((year % 4 == 0)&&((year % 100 != 0)||(year % 400 == 0))){
            System.out.println("This year is leap!");
        }
        else {
            System.out.println("This year isn't leap!");
        }

    }
}
