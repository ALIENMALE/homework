package CustomEnum;

public class Cashback {
    private int code;
    private String message;
    private float percent;

    public Cashback(int code, String message, float percent) {
        this.code = code;
        this.message = message;
        this.percent = percent;
    }

    public static final Cashback CINEMA = new Cashback(1,
            "билеты в кино", 5);
    public static final Cashback PHARMACY = new Cashback(2,
            "покупки в аптеке", 10);
    public static final Cashback TAXI = new Cashback(3,
            "поездки в такси", 3.5f);
    public static final Cashback CLOTHES = new Cashback(4,
            "покупку одежды", 4);
    public static final Cashback FOOD = new Cashback(5,
            "покупку еды", 3);
    public static final Cashback GAMES = new Cashback(6,
            "покупку игр", 7.5f);
    public static final Cashback SPORT = new Cashback(7,
            "покупку спорттоваров", 8);
    public static final Cashback TRAVEL = new Cashback(8,
            "покупку тура", 3);

    @Override
    public String toString() {
        return "Поздравляем! Вы получили кэшбэк за " + message + "! Процент кэшбэка составил: " + percent;
    }
}