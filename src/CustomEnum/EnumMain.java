package CustomEnum;

import java.util.Scanner;

public class EnumMain {

    public static void main(String[] args) {
        System.out.println("Выберете категорию кэшбэка:" +
                "\n 1. Кино" +
                "\n 2. Аптека" +
                "\n 3. Такси" +
                "\n 4. Одежда" +
                "\n 5. Еда" +
                "\n 6. Игры" +
                "\n 7. Спорттовары" +
                "\n 8. Путешествия");
        Scanner in = new Scanner(System.in);
        int input = in.nextInt();
        switch (input) {
            case 1 -> System.out.println(Cashback.CINEMA);
            case 2 -> System.out.println(Cashback.PHARMACY);
            case 3 -> System.out.println(Cashback.TAXI);
            case 4 -> System.out.println(Cashback.CLOTHES);
            case 5 -> System.out.println(Cashback.FOOD);
            case 6 -> System.out.println(Cashback.GAMES);
            case 7 -> System.out.println(Cashback.SPORT);
            case 8 -> System.out.println(Cashback.TRAVEL);
            default -> System.out.println("Такой категории нет!");
        }
    }
}
