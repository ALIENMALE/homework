package Patterns.Singleton;

public class LoggerMain {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance("FirstClass", "SecondClass");
        System.out.println(logger.toString());
    }
}
