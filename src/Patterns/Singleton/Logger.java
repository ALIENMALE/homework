package Patterns.Singleton;

public class Logger {

    private static Logger INSTANCE;
    public String className;
    public String addtInfo;

    private Logger(String className, String addtInfo) {
        this.className = className;
        this.addtInfo = addtInfo;
    }

    public static Logger getInstance(String className, String addtInfo) {
        if (INSTANCE == null) {
            INSTANCE = new Logger(className, addtInfo);
        }
        return INSTANCE;
    }

    @Override
    public String toString() {
        return "log info: " + className + ", " + addtInfo;
    }
}
