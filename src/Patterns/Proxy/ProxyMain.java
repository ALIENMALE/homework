package Patterns.Proxy;

public class ProxyMain {
    public static void main(String[] args) {
        Database database = new Database();
        ProxyConnect proxyConnect = new ProxyConnect(database);

        proxyConnect.connect();
    }

}
