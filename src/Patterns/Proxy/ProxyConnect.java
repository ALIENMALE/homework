package Patterns.Proxy;

public class ProxyConnect {

    private Database database;

    public ProxyConnect(Database database){
        this.database = database;
    }

    public void connect(){
        System.out.println("localhost:port/" + database.connect());
    }

}
