package Patterns.Adapter;

public interface DataStorageDevice {

    public String getInfo();
}
