package Patterns.Adapter;

public class Computer {

    public void readUSB (DataStorageDevice dataStorageDevice){
        System.out.println("Got it! Info from data storage device: " + dataStorageDevice.getInfo());
    }
}
