package Patterns.Adapter;

public class USB implements DataStorageDevice{

    @Override
    public String getInfo(){
        return "Info from USB-device";
    }
}
