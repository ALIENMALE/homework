package Patterns.Adapter;

public class MainAdapter {

    public static void main(String[] args) {

        USB usb = new USB();

        Computer computer = new Computer();
        computer.readUSB(usb);

        MemCard memCard = new MemCard();
        MemCardAdapter memCardAdapter = new MemCardAdapter(memCard);
        computer.readUSB(memCardAdapter);
    }
}
