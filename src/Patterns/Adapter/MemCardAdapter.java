package Patterns.Adapter;

public class MemCardAdapter implements DataStorageDevice {
    protected MemCard memCard;

    public MemCardAdapter (MemCard memCard){
        this.memCard = memCard;
    }

    public String getInfo() {
        return memCard.getCardInfo();
    }
}
