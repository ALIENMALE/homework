package Patterns.Factory;

public class Americano extends Coffee{

    private boolean espresso;
    private boolean water;

    public Americano (boolean espresso, boolean water){
        this.espresso = espresso;
        this.water = water;
    }

    @Override
    public boolean getEspresso() {
        return this.espresso;
    }

    public boolean getWater() {
        return this.water;
    }
}
