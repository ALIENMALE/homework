package Patterns.Factory;

public class CoffeeFactory {

    public static Coffee getCoffee(String type){
        if ("espresso".equalsIgnoreCase(type)) return new Espresso(true, false);
        else if ("americano".equalsIgnoreCase(type)) return new Americano(true,true);

        return null;
    }
}
