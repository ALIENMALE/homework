package Patterns.Factory;

public class CoffeeMain {

    public static void main(String[] args) {
        Coffee espresso = CoffeeFactory.getCoffee("espresso");
        Coffee americano = CoffeeFactory.getCoffee("americano");

        System.out.println("What's inside an espresso cup?\n" + espresso);
        System.out.println("What's inside an americano cup?\n" + americano);
    }
}
