package Patterns.Factory;

public abstract class Coffee {

    public abstract boolean getEspresso();
    public abstract boolean getWater();

    @Override
    public String toString() {
        return "Is there an espresso in this cup? " + this.getEspresso() + "\nIs there water in this cup? " + this.getWater() +
                "\n------------------------------------";
    }
}
