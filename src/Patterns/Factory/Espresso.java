package Patterns.Factory;

public class Espresso extends Coffee {

    private boolean espresso;
    private boolean water;

    public Espresso(boolean espresso, boolean water){
        this.espresso = espresso;
        this.water = water;
    }

    @Override
    public boolean getEspresso() {
        return this.espresso;
    }

    @Override
    public boolean getWater() {
        return this.water;
    }
}
